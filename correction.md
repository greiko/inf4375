# Correction

#### TP - INF4375
#### NICOLAS PAPANICOLAOU
#### PAPN20098909

## Installation

Installer:
- Node.js 8.6 ou plus
- Express.js 4 ou plus
- MongoBD 3.4 ou plus
    
Par la suite

#### Sous linux:

Pour installer les librairies necessaires:
```
npm install
```

Pour importer les donnees dans la BD de MongoDB:
```
npm run importationDonnees
```

Pour commencer le serveur:
```
npm start
```


## Fonctionnalites developpes

|A|C|D|E|
|:---:|:----:|:----:|:----:|
|[[A1]](#A1)|[[C1]](#C1)|[[D1]](#D1)|[[E1]](#E1)
|[[A2]](#A2)|[[C2]](#C2)|[[D2]](#D2)|[[E2]](#E2)
|[[A3]](#A3)|[[C3]](#C3)|[[D3]](#D3)|
|[[A4]](#A4)|   |     | 
|[[A5]](#A5)|   |     |
|[[A6]](#A6)|   |     |



<a name="A1"></a>
### A1
Ouvrir mongodb dans un terminal

Sous linux
```
mongo
```
Dans le terminal de mongodb saisissez ceci:
```
use tp
show collections
```

Si vous voyez
```
glissades
patinoires
piscines
```
La fonctionnalite A1 est fonctionnelle


<a name="A2"></a>
### A2
En laissant rouler l'application. A minuit, vous verez que la fonctionnalite A1 est appelle donc A2 est fonctionnelle.


<a name="A3"></a>
### A3
Acceder `localhost:3001/doc` pour les services REST de mon TP

Une html est genere avec la librairie `raml2html`.

<a name="A4"></a>
### A4
Dans un fureteur, veuillez saisir `localhost:3001/installations?arrondissement="Le Sud-Ouest"`.
Vous allez recevoir en format JSON la liste des installations dans l'arrondissement specifie.

<a name="A5"></a>
### A5
Dans la page d'accueil `localhost:3001`, vous avec un champs que vous pouvez saisir l'arrondissement de votre choix. En recherchant, cette fonctionnalitees fait appel au service REST en A4 pour recevoir le JSON. Par la suite, avec JQuery et AJAX, une table est forme avec les noms des installations de l'arrondissement specifie.

<a name="A6"></a>
### A6
Apres d'avoir choisi l'arrondissement, vous pouvez selectionne une installation par sont nom parmi une liste deroulante concu avec votre recherche en A5. Vous allez pouvoir voir l'information de cette installation.

<a name="C1"></a>
### C1
Dans un fureteur, veuillez saisir `localhost:3001/installations/mauvaisesConditionsJSON"`.
Vous allez recevoir en format JSON la liste des installations en mauvaises conditions.

<a name="C2"></a>
### C2
Dans un fureteur, veuillez saisir `localhost:3001/installations/mauvaisesConditionsXML"`.
Vous allez recevoir en format XML la liste des installations en mauvaises conditions.

<a name="C3"></a>

### C3
Dans un fureteur, veuillez saisir `localhost:3001/installations/mauvaisesConditionsCSV"`.
Vous allez recevoir en format CSV la liste des installations en mauvaises conditions.

<a name="D1"></a>

### D1
Vous pouvez tester cette fonctionnalite soit:
* En faisant un requete `PUT` en suivant la documentation `localhost:3001/doc`
* Via `localhost:3001/`
    - Apres d'avoir afficher la liste des installations d'un arrondissement specifier, vous pouvez modifier l'etat de la glissade.
Le JSON envoye avec la requete est verifier par un json-schema.


<a name="D2"></a>
### D2
Vous pouvez tester cette fonctionnalite soit:
* En faisant un requete `DELETE` en suivant la documentation `localhost:3001/doc`
* Via `localhost:3001/`
    - Apres d'avoir afficher la liste des installations d'un arrondissement specifier, vous pouvez supprimer la glissade.


<a name="D3"></a>
### D3
D1 et D2 sont inclus dans l'application fait en A5 en rajoutant les services REST pour les patinoires et les piscines.
- Apres d'avoir afficher la liste des installations d'un arrondissement specifier, vous pouvez modifier l'etat ou supprimer de l'installation.


<a name="E1"></a>
### E1
En suivant la documentation `localhost:3001/doc` pour le service REST, vous pouvez alors creer un utilisateur.

<a name="E2"></a>
Le JSON envoye avec la requete est verifier par un json-schema.
### E2
La fonctionnalitee en E1 est implementer sur un page html qui utiliser le service REST de E1
