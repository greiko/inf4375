var raml2html = require('raml2html');
var express = require('express');
var router = express.Router();

const config = raml2html.getConfigForTheme();

var ramlSource = "./views/doc.raml";


router.get('/', function (req, res) {
    var onError = function (err) {
        console.log(err);
        res.sendStatus(500);
    };
    var onSuccess = function (html) {
        res.send(html);
    };
    raml2html.render(ramlSource, config).then(onSuccess, onError);
});

module.exports = router;
