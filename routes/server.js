var mongo = require("mongodb");
var xml2js = require("xml2js");
var csvtojson = require("csvtojson");
var schedule = require("node-schedule");
var request = require('request');

exports.importationMinuit = function () {
    schedule.scheduleJob('0 0 * * *', function () {
        var date = new Date();
        console.log("Update de la BD" + date.toTimeString());
        importationPatinoires();
        importationGlissades();
        importationPiscines();
    });
};

exports.importationDonnees = function () {
    importationPatinoires();
    importationGlissades();
    importationPiscines();
};

function insererBD(list, nameOfCollections) {
    var server = new mongo.Server("localhost", 27017);
    var db = new mongo.Db("tp", server, {safe: true});

    db.open(function (error, db) {
        if (error) {
            console.log("Erreur de connexion avec BD", error);
        } else {
            db.collection(nameOfCollections).drop(function (error, reponse) {
                if (error) {
                    console.log("Collection n'existe pas");
                } else {
                }
            });
            db.collection(nameOfCollections, function (error, collection) {
                if (error) {
                    console.log("Erreur avec la collection", error);
                    db.close();
                } else {
                    collection.insert(list, function (error, result) {
                        if (error) {
                            console.log("Erreur avec l'insertion", error);
                        }
                        else {
                            console.log("Insertion dans " + nameOfCollections);

                        }

                        db.close();
                    });
                }
            });
        }
    });
}


function importationPatinoires() {


    request('http://www2.ville.montreal.qc.ca/services_citoyens/pdf_transfert/L29_PATINOIRE.xml', function (err, res, body) {

        var parser = new xml2js.Parser();
        var json = "";

        parser.parseString(body.substring(0, body.length), function (err, result) {
            json = JSON.parse(JSON.stringify(result));
        });


        var patinoires = json.patinoires.patinoire;

        insererBD(patinoires, "patinoires");
    });
}

function importationGlissades() {


    request('http://www2.ville.montreal.qc.ca/services_citoyens/pdf_transfert/L29_GLISSADE.xml', function (err, res, body) {

        var parser = new xml2js.Parser();
        var json = "";

        parser.parseString(body.substring(0, body.length), function (err, result) {
            json = JSON.parse(JSON.stringify(result));
        });

        var glissades = json.glissades.glissade;

        insererBD(glissades, "glissades");
    });


}

function importationPiscines() {


    var piscines = [];
    request('http://donnees.ville.montreal.qc.ca/dataset/4604afb7-a7c4-4626-a3ca-e136158133f2/resource/cbdca706-569e-4b4a-805d-9af73af03b14/download/piscines.csv', function (err, res, body) {
        csvtojson()
            .fromString(body)
            .on("json", function (jsonObj) {
                piscines.push(jsonObj)
            });

        insererBD(piscines, "piscines");
    });


}
