var express = require('express');
var router = express.Router();
var mongo = require("mongodb");
var json2csv = require("json2csv")

var server = new mongo.Server("localhost", 27017);
var db = new mongo.Db("tp", server, {safe: true});


router.get('/', function (req, res) {

    var arrondissement = req.query.arrondissement;
    if (!arrondissement){
        res.status(400);
        res.send({message: "Mauvaise requete"});
        return;
    }
    var requeteXML = ({arrondissement: {$elemMatch: {nom_arr: arrondissement}}});
    var requeteCSV = {ARRONDISSE: arrondissement};


    db.open(function (err, db) {
        if (err) throw err;
        var json = "";
        res.setHeader('Content-Type', 'application/json');
        var glissades = "{ \"glissades\":";
        var patinoires = ", \"patinoires\":";
        var piscines = ", \"piscines\":";

        db.collection("glissades").find(requeteXML).toArray(function (err, result) {
            if (err) throw err;

            json = glissades.concat(JSON.stringify(result));

            db.collection("patinoires").find(requeteXML).toArray(function (err, result) {
                if (err) throw err;

                json = json.concat(patinoires);
                json = json.concat(JSON.stringify(result));

                db.collection("piscines").find(requeteCSV).toArray(function (err, result) {
                    if (err) throw err;

                    json = json.concat(piscines);
                    json = json.concat(JSON.stringify(result));
                    json = json.concat("}");

                    res.status(200);
                    res.send(json);
                    db.close();
                });
            });
        });
    });
});


router.get('/mauvaisesConditions/:media', function (req, res) {

    var media = req.params.media;
    if (media == "xml") {
        mauvaisesConditionsXML(res);
    }
    else if (media == "json"){
        mauvaisesConditionsJSON(res);

    }
    else if (media == "csv"){
        mauvaisesConditionsCSV(res);

    }
    else {
        res.status(400);
        res.send("Media non reconnu");
    }

});

function mauvaisesConditionsJSON (res) {

    var query = {condition: "Mauvaise"};
    var sortQuery = {nom: 1};


    db.open(function (err, db) {
        if (err) throw err;
        var json = "";
        var glissades = "{ \"glissades\":";
        var patinoires = ", \"patinoires\":";

        db.collection("glissades").find(query).sort(sortQuery).toArray(function (err, result) {
            if (err) throw err;

            json = glissades.concat(JSON.stringify(result));

            db.collection("patinoires").find(query).sort(sortQuery).toArray(function (err, result) {
                if (err) throw err;

                json = json.concat(patinoires);
                json = json.concat(JSON.stringify(result));
                json = json.concat("}");

                db.close();
                res.setHeader('Content-Type', 'application/json', 'charset=utf-8');
                res.status(200);
                res.send(json);

            });
        });
    });
}

function mauvaisesConditionsXML(res) {
    var requeteCondition = {condition: "Mauvaise"};
    var requeteTriage = {nom: 1};
    var xml = "";

    var xmlVersion = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    var xmlRoot = "<root>";

    db.open(function (err, db) {
        if (err) throw err;
        xml = xml.concat(xmlVersion);
        xml = xml.concat(xmlRoot);
        var glissades = "<glissades>";
        var patinoires = "<patinoires>";

        db.collection("glissades").find(requeteCondition).sort(requeteTriage).toArray(function (err, result) {
            if (err) throw err;
            xml = xml.concat(glissades);

            for (var i = 0, len = result.length; i < len; i++) {
                xml = xml.concat("<glissade>");
                xml = xml.concat("<_id>" + result[i]._id + "</_id>");
                xml = xml.concat("<nom>" + result[i].nom + "</nom>");
                xml = xml.concat("<arrondissement>");
                xml = xml.concat("<nom_arr>" + result[i].arrondissement.nom_arr + "</nom_arr>");
                xml = xml.concat("<cle>" + result[i].arrondissement.cle + "</cle>");
                xml = xml.concat("<date_maj>" + result[i].arrondissement.date_maj + "</date_maj>");
                xml = xml.concat("</arrondissement>");
                xml = xml.concat("<ouvert>" + result[i].ouvert + "</ouvert>");
                xml = xml.concat("<deblaye>" + result[i].deblaye + "</deblaye>");
                xml = xml.concat("<condition>" + result[i].condition + "</condition>");

                xml = xml.concat("</glissade>");

            }

            xml = xml.concat("</glissades>");

            db.collection("patinoires").find(requeteCondition).sort(requeteTriage).toArray(function (err, result) {
                if (err) throw err;

                xml = xml.concat(patinoires);

                for (var i = 0, len = result.length; i < len; i++) {
                    xml = xml.concat("<patinoire>");
                    xml = xml.concat("<_id>" + result[i]._id + "</_id>");
                    xml = xml.concat("<nom>" + result[i].nom + "</nom>");
                    xml = xml.concat("<arrondissement>");
                    xml = xml.concat("<nom_arr>" + result[i].arrondissement.nom_arr + "</nom_arr>");
                    xml = xml.concat("<cle>" + result[i].arrondissement.cle + "</cle>");
                    xml = xml.concat("<date_maj>" + result[i].arrondissement.date_maj + "</date_maj>");
                    xml = xml.concat("</arrondissement>");
                    xml = xml.concat("<ouvert>" + result[i].ouvert + "</ouvert>");
                    xml = xml.concat("<deblaye>" + result[i].deblaye + "</deblaye>");
                    xml = xml.concat("<arrose>" + result[i].arrose + "</arrose>");
                    xml = xml.concat("<resurface>" + result[i].resurface + "</resurface>");
                    xml = xml.concat("<condition>" + result[i].condition + "</condition>");

                    xml = xml.concat("</patinoire>");
                }
                xml = xml.concat("</patinoires>");
                xml = xml.concat("</root>");

                db.close();
                res.setHeader('Content-Type', 'application/xml');
                res.status(200);
                res.send(xml);
            });
        });
    });
}


function mauvaisesConditionsCSV(res) {

    var requeteCondition = {condition: "Mauvaise"};
    var requeteTriage = {nom: 1};

    var glissadesHeader = [
        "_id",
        "nom",
        "nom_arr",
        "cle",
        "date_maj",
        "ouvert",
        "deblaye",
        "condition"
    ];

    var patinoiresHeader = [
        "_id",
        "nom",
        "nom_arr",
        "cle",
        "date_maj",
        "ouvert",
        "deblaye",
        "arrose",
        "resurface",
        "condition"
    ];

    db.open(function (err, db) {
        if (err) throw err;
        res.setHeader('Content-Type', 'text/csv', 'charset=utf-8');
        db.collection("glissades").find(requeteCondition).sort(requeteTriage).toArray(function (err, result) {
            if (err) throw err;

            var csvResult = "GLISSADES\n";
            var glissadesResultCsv = json2csv({data: result, fields: glissadesHeader});
            csvResult = csvResult.concat(glissadesResultCsv);


            db.collection("patinoires").find(requeteCondition).sort(requeteTriage).toArray(function (err, result) {
                if (err) throw err;

                csvResult = csvResult.concat("\nPATINOIRES\n");
                var csvR = json2csv({data: result, fields: patinoiresHeader});
                csvResult = csvResult.concat(csvR);

                db.close();
                res.setHeader('Content-Type', 'text/csv');
                res.status(200);
                res.send(csvResult);
            });
        });
    });
}



router.get('/:nomInstallation', function (req, res) {

    var nom = req.params.nomInstallation;
    var requeteXML = {nom: nom};
    var requeteCSV = {NOM: nom};

    db.open(function (err, db) {
        if (err) throw err;
        res.setHeader('Content-Type', 'text/html');
        var data = {};

        db.collection("glissades").findOne(requeteXML, function (err, result) {
            if (err) throw err;

            if (result) {
                data["glissade"] = result;
                res.render("infoInstallation", data);
            }

            db.collection("patinoires").findOne(requeteXML, function (err, result) {
                if (err) throw err;

                if (result) {
                    data["patinoire"] = result;
                    res.render("infoInstallation", data);
                }


                db.collection("piscines").findOne(requeteCSV, function (err, result) {
                    if (err) throw err;


                    if (result) {
                        data["piscine"] = result;
                        res.render("infoInstallation", data);
                    }

                    db.close();
                });
            });
        });
    });
});



module.exports = router;
