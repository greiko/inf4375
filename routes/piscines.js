var express = require('express');
var router = express.Router();
var mongo = require("mongodb");
var Validator = require("jsonschema").Validator;

var server = new mongo.Server("localhost", 27017);
var db = new mongo.Db("tp", server, {safe: true});


router.put('/:nomPiscine', function (req, res) {
    var nom = req.params.nomPiscine;
    var propriete = req.body.propriete;
    var gestion = req.body.gestion;
    var equipeme = req.body.equipeme;
    var data = {};
    if (propriete) data["PROPRIETE"] = propriete;
    if (gestion) data["GESTION"] = gestion;
    if (equipeme) data["EQUIPEME"] = equipeme;


    var schema =
        {
            "properties": {
                "propriete": {
                    "type": "string"
                },
                "equipeme": {
                    "type": "string"

                },
                "gestion": {
                    "type": "string"
                }
            }
        };


    var validator = new Validator();
    var validation = validator.validate(req.body, schema);
    if (validation.errors.length > 0) {
        res.status(400);
    }
    else {
        db.open(function (err, db) {
            if (err) throw err;

            db.collection("piscines", function (err, collection) {
                collection.update({"NOM": nom}, {
                    $set: data
                }, function (err, result) {
                    db.close();
                });
            });

        });
        res.status(200);
    }

    res.send();
});

router.delete('/:nomPiscine', function (req, res) {
    var nom = req.params.nomPiscine;


    db.open(function (err, db) {
        if (err) throw err;

        db.collection("piscines", function (err, collection) {
            collection.remove({"NOM": nom}
                , function (err, result) {
                    if (err) {
                        res.status(400);
                    }
                    else {
                        res.status(200);
                    }
                    db.close();
                });
        });

    });

    res.send();
});

module.exports = router;
