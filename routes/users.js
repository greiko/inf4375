var express = require('express');
var router = express.Router();
var mongo = require("mongodb");
var Validator = require("jsonschema").Validator;
var server = new mongo.Server("localhost", 27017);
var db = new mongo.Db("tp", server, {safe: true});

router.get('/', function (req, res) {
    res.render("users");
});

router.post('/', function (req, res) {
    var prenom = req.body.prenom;
    var nomDeFamille = req.body.nomDeFamille;
    var courriel = req.body.courriel;
    var arrondissements = req.body.arrondissements;



    var fs = require("fs");
    var schemaFile = fs.readFileSync("./schemas/userSchema.json");
    var schema = JSON.parse(schemaFile);


    var validator = new Validator();
    var validation = validator.validate(req.body, schema);
    if (validation.errors.length > 0) {
        console.log("Erreur JSONSCHEMA");
        res.status(400);
    }
    else {
        sendToDB(req.body);
        res.status(200);
    }
    res.send();
});

function sendToDB(user) {

    db.open(function (error, db) {
        if (error) {
            console.log("Erreur de connexion avec BD", error);
        } else {
            db.collection("users", function (error, collection) {
                if (error) {
                    console.log("Erreur avec la collection", error);
                    db.close();
                } else {
                    collection.insert(user, function (error, result) {
                        if (error) {
                            console.log("Erreur avec l'insertion", error);
                        }
                        else {
                        }
                        db.close();
                    });
                }
            });
        }
    });
}

module.exports = router;
