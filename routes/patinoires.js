var express = require('express');
var router = express.Router();
var mongo = require("mongodb");
var Validator = require("jsonschema").Validator;

var server = new mongo.Server("localhost", 27017);
var db = new mongo.Db("tp", server, {safe: true});


router.put('/:nomPatinoires', function (req, res) {
    var nom = req.params.nomPatinoires;
    var cle = req.body.cle;
    var dateMaj = req.body.dateMaj;
    var ouvert = req.body.ouvert;
    var deblaye = req.body.deblaye;
    var arrose = req.body.arrose;
    var resurface = req.body.resurface;
    var condition = req.body.condition;

    var data = {};
    if (cle) data["arrondissement.$.cle.0"] = cle;
    if (dateMaj) data["arrondissement.$.date_maj.0"] = dateMaj;
    if (ouvert) data["ouvert"] = ouvert;
    if (deblaye) data["deblaye"]  = deblaye;
    if (arrose) data["arrose"]  = arrose;
    if (resurface) data["resurface"]  = resurface;
    if (condition) data["condition"] = condition;


    var schema =
        {
            "properties": {
                "cle": {
                    "type": "string"
                },
                "dateMaj": {
                    "type": "string",
                    "pattern": "[0-9]{4}\\-([0,9]|0[0-9]|1[0-2])\\-(3[0,1]|[0-2][0-9]|[0-9])( ([0-9]|0[0-9]|1[0-9]):[0-9]{2}:[0-9]{2})?|^$"

                },
                "ouvert": {
                    "type": "string"

                },
                "deblaye": {
                    "type": "string"

                },
                "arrose": {
                    "type": "string"

                },
                "resurface": {
                    "type": "string"

                },
                "condition": {
                    "type": "string"

                }

            }
        };


    var validator = new Validator();
    var validation = validator.validate(req.body, schema);
    if (validation.errors.length > 0) {
        res.status(400);
    }
    else {
        db.open(function (err, db) {
            if (err) throw err;

            db.collection("patinoires", function (err, collection) {
                collection.update({"nom": nom}, {
                    $set: data
                }, function (err, result) {
                    db.close();
                });
            });

        });
        res.status(200);
    }

    res.send();
});

router.delete('/:nomPatinoires', function (req, res) {
    var nom = req.params.nomPatinoires;

    db.open(function (err, db) {
        if (err) throw err;

        db.collection("patinoires", function (err, collection) {
            collection.remove({"nom": nom}
                , function (err, result) {
                    if (err) {
                        res.status(400);
                    }
                    else {
                        res.status(200);
                    }
                    db.close();
                });
        });

    });

    res.send();
});

module.exports = router;
