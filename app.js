var installations = require('./routes/installations');
var documentations = require('./routes/doc');
var index = require('./routes/index');
var users = require('./routes/users');
var glissades = require('./routes/glissades');
var patinoires = require('./routes/patinoires');
var piscines = require('./routes/piscines');
var server = require("./routes/server");
var bodyParser = require('body-parser');
var express = require('express');
var path = require('path');

var app = express();


app.set('port', process.env.PORT || 3001);
app.set('views', __dirname + '/views');
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


app.use('/utilisateurs', users);
app.use('/installations', installations);
app.use('/doc', documentations);
app.use('/glissades', glissades);
app.use('/piscines', piscines);
app.use('/patinoires', patinoires);
app.use('/', index);

app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
    server.importationMinuit();
});

module.exports = app;

