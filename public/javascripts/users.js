function creationUtilisateur() {

    var request = new XMLHttpRequest();

    var prenom = document.getElementById("firstName").value;
    var nomDeFamille = document.getElementById("lastName").value;
    var courriel = document.getElementById("email").value;
    var arrondissements = document.getElementById("arrondissements").value;

    if (!prenom || !nomDeFamille || !courriel) {
        return;
    }

    var url = "/utilisateurs";
    var arrArray;
    if (arrondissements) {
        arrArray = arrondissements.split("\n");
    }
    var data = {
        prenom: prenom,
        nomDeFamille: nomDeFamille,
        courriel: courriel,
        arrondissements: arrArray
    };

    request.open("POST", url);
    request.setRequestHeader("Content-type", "application/json");

    request.onreadystatechange = function () {
        var body = document.getElementById("CreationDone");
        if (request.readyState === 4 && request.status === 200) {
            body.innerHTML = "<div class=\"alert alert-success\">Creation utilisateur reussi</div>";

            document.getElementById("firstName").value = "";
            document.getElementById("lastName").value = "";
            document.getElementById("email").value = "";
            document.getElementById("arrondissements").value = "";
        }

        if (request.readyState === 4 && request.status === 400) {
            body.innerHTML = "<div class=\"alert alert-danger\">Pas conforme au JSONSCHEMA</div>";
        }
    }

    request.send(JSON.stringify(data));
    return false;
}