$(document).ready(function () {
    $("#btnArron").click(function (event) {
        event.preventDefault();
        var name = document.getElementById("name").value;
        $.ajax({
            url: "/installations?arrondissement=" + name,
            type: "get",
            datatype: "json",
            success: function (jsonObject, textStatus, xmlHttpRequest) {

                var glissades = jsonObject["glissades"];
                var patinoires = jsonObject["patinoires"];
                var piscines = jsonObject["piscines"];
                var glissadesHeader = [
                    "Nom"
                ];
                var patinoiresHeader = [
                    "Nom"
                ];
                var piscinesHeader = [
                    "Nom"
                ];
                var html = "";

                html += "<div class='col-lg-8 col-lg-offset-12'><div class='col-lg-12" +
                    "<form class='form-control' onsubmit='return false'>" +
                    "<label>Recherche par nom d'installation</label>" +
                    "<select class='form-control custom-select' id='selectInstallation' required=''>" +
                    "<option selected disabled>Choisir parmi cette liste</option>";


                html += "<optgroup label='Glissades'>";
                glissades.forEach(function (glissade) {
                    html += "<option>" + glissade.nom + "</option>"
                });

                html += "<optgroup label='Patinoires'>";
                patinoires.forEach(function (patinoire) {
                    html += "<option>" + patinoire.nom + "</option>"
                });

                html += "<optgroup label='Piscines'>";
                piscines.forEach(function (piscine) {
                    html += "<option>" + piscine.NOM + "</option>"
                });


                html += "</select>" +
                    "<button id='btnArron' class='btn btn-primary' type='submit' onclick='getInfoInstallation()'>Rechercher</button>" +
                    "</form>";
                html += "<div class='col-lg-12' id='rechercheInstallation'></div>";
                html += "<div class='col-lg-12' id='resultatButton'></div>";

                html += "<table id='installations' class='table table-hover table-striped table-bordered'>";

                html += "<thead><tr><th class='tTitre' colspan='8'>Glissades</th></tr><tr>";
                glissadesHeader.forEach(function (header) {
                    html += "<th>" + header + "</th>";
                });

                html += "</tr></thead>";
                html += "<tbody>";
                if (glissades.length > 0) {
                    glissades.forEach(function (glissade) {
                        html += "<tr>" +
                            "<td>" + glissade.nom + "</td>";
                    });
                }
                else {
                    html += "<td colspan='8'> Aucunes Donnee </td>";
                }
                html += "</tbody>";

                html += "<thead><tr><th class='tTitre' colspan='10'>Patinoires</th></tr><tr>";
                patinoiresHeader.forEach(function (header) {
                    html += "<th>" + header + "</th>";
                });

                html += "</tr></thead>";
                html += "<tbody>";
                if (patinoires.length > 0) {
                    patinoires.forEach(function (patinoire) {
                        html += "<tr>" +
                            "<td>" + patinoire.nom + "</td>";
                    });
                }
                else {
                    html += "<td colspan='10'> Aucunes Donnee </td>";
                }
                html += "</tbody>";

                html += "<thead><tr><th class='tTitre' colspan='12'>Piscines</th></tr><tr>";
                piscinesHeader.forEach(function (header) {
                    html += "<th>" + header + "</th>";
                });

                html += "</tr></thead>";
                html += "<tbody>";
                if (piscines.length > 0) {
                    piscines.forEach(function (piscine) {
                        html += "<tr>" +
                            "<td>" + piscine.NOM + "</td>";
                    });
                }
                else {
                    html += "<td colspan='12'> Aucunes Donnee </td>";
                }
                html += "</tbody>";
                html += "</table>";

                var myTable = document.getElementById("myTable");
                myTable.innerHTML = html;
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                if (xmlHttpRequest.status === 400) {
                }
            }

        });
    });

});


function getInfoInstallation() {
    var request = new XMLHttpRequest();

    var name = document.getElementById("selectInstallation").value;
    var url = "/installations/" + name;

    request.open("GET", url);
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            var tbody = document.getElementById("rechercheInstallation");
            tbody.innerHTML = request.responseText;
        }

    };

    request.send(request.responseText);


}


function modifierInstallation() {
    var request = new XMLHttpRequest();
    var selectInstallation = document.getElementById("selectInstallation");
    var name = document.getElementById("selectInstallation").value;
    var optGroup = selectInstallation.options[selectInstallation.selectedIndex].parentNode.label;
    var data = {};
    var url = "";
    if (optGroup == "Glissades") {
        url += "/glissades/"
        var cle = document.getElementById("cle").value;
        var dateMaj = document.getElementById("dateMaj").value;
        var ouvert = document.getElementById("ouvert").value;
        var deblaye = document.getElementById("deblaye").value;
        var condition = document.getElementById("condition").value;
        data = {
            cle: cle,
            dateMaj: dateMaj,
            ouvert: ouvert,
            deblaye: deblaye,
            condition: condition
        };
    } else if (optGroup == "Patinoires") {
        url += "/patinoires/"
        var cle = document.getElementById("cle").value;
        var dateMaj = document.getElementById("dateMaj").value;
        var ouvert = document.getElementById("ouvert").value;
        var deblaye = document.getElementById("deblaye").value;
        var arrose = document.getElementById("arrose").value;
        var resurface = document.getElementById("resurface").value;
        var condition = document.getElementById("condition").value;
        data = {
            cle: cle,
            dateMaj: dateMaj,
            ouvert: ouvert,
            deblaye: deblaye,
            arrose: arrose,
            resurface: resurface,
            condition: condition
        }
    } else if (optGroup == "Piscines") {
        url += "/piscines/"
        var propriete = document.getElementById("propriete").value;
        var gestion = document.getElementById("gestion").value;
        var equipeme = document.getElementById("equipeme").value;
        data = {
            propriete: propriete,
            gestion: gestion,
            equipeme: equipeme
        }

    }
    url += name;
    request.open("PUT", url);
    request.setRequestHeader("Content-type", "application/json");
    var body = document.getElementById("resultatButton");

    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            body.innerHTML = "<div class=\"alert alert-success\">Modification de la glissade reussi</div>";
            document.getElementById("rechercheInstallation").innerHTML = "";
            $('#resultatButton').stop().fadeIn('fast')
            $('#resultatButton').fadeOut(3000);

        }
        if (request.readyState === 4 && request.status === 400) {
            body.innerHTML = "<div class=\"alert alert-danger\">Pas conforme au JSONSCHEMA</div>";
            $('#resultatButton').stop().fadeIn('fast')
            $('#resultatButton').fadeOut(3000);
        }

    };

    request.send(JSON.stringify(data));
}

function supprimerInstallation() {
    var request = new XMLHttpRequest();

    var selectInstallation = document.getElementById("selectInstallation");
    var name = document.getElementById("selectInstallation").value;
    var optGroup = selectInstallation.options[selectInstallation.selectedIndex].parentNode.label;
    var url = "";
    if (optGroup == "Glissades") {
        url += "/glissades/"
    } else if (optGroup == "Patinoires") {
        url += "/patinoires/"

    } else if (optGroup == "Piscines") {
        url += "/piscines/"

    }
    url += name;

    request.open("DELETE", url);

    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            var body = document.getElementById("resultatButton");
            body.innerHTML = "<div class=\"alert alert-success\">Supprimmer l'installation reussi</div>";
            document.getElementById("rechercheInstallation").innerHTML = "";
            $('#resultatButton').stop().fadeIn('fast')
            $('#resultatButton').fadeOut(3000);
            for (var i = 0; i < selectInstallation.length; i++) {
                if (selectInstallation.options[i].value == name) {
                    selectInstallation.remove(i);
                }


            }

            var installations = document.getElementById("installations");
            for (var i = 0; i < installations.rows.length; i++) {
                var installation = installations.rows[i].cells[0].innerHTML;
                if (installation == name) {
                    installations.deleteRow(i);
                    break;
                }
            }


        }
        if (request.readyState === 4 && request.status === 400) {
            body.innerHTML = "<div class=\"alert alert-danger\">Mauvaise Requete/div>";
            $('#resultatButton').stop().fadeIn('fast')
            $('#resultatButton').fadeOut(3000);
        }

    };

    request.send();
}